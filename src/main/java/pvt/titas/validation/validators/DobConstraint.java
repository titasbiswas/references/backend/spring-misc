package pvt.titas.validation.validators;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DobConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DobConstraint {
    int ageGreaterThan() default 18;

    String message() default "Age should be more than 18";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
