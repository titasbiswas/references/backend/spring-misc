package pvt.titas.validation.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

public class DobConstraintValidator implements ConstraintValidator<DobConstraint, Date> {

    private int validationAge;

    @Override
    public void initialize(DobConstraint constraintAnnotation) {
        this.validationAge = constraintAnnotation.ageGreaterThan();
    }

    @Override
    public boolean isValid(Date value, ConstraintValidatorContext context) {
        if(value==null)
            return false;
        LocalDate dob = value.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int gap = Period.between(dob, LocalDate.now()).getYears();
        return gap >= validationAge;
    }
}
