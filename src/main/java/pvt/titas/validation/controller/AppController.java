package pvt.titas.validation.controller;

import org.springframework.web.bind.annotation.*;
import pvt.titas.validation.domain.Customer;
import pvt.titas.validation.repository.CustomerRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/spring-validation")
public class AppController {

    private CustomerRepository customerRepository;

    public AppController(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }

    @GetMapping("/customer")
    public List<Customer> getAllCustomer(){
        return customerRepository.findAll();
    }

    @PostMapping("/customer")
    public Customer createCustomer(@Valid @RequestBody Customer customer){
        return customerRepository.save(customer);
    }

}
