package pvt.titas.validation.domain;

import lombok.Data;
import pvt.titas.validation.validators.DobConstraint;
import pvt.titas.validation.validators.PhoneNumberConstraint;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "Name can't be empty")
    private String name;

    @NotBlank(message = "email can't be empty")
    @Email(message = "Please provide a proper email id")
    private String email;

    @PhoneNumberConstraint(message = "Please provide a valid phone number")
    private String phone;


    @NotNull
    @DobConstraint
    private Date dob;

    @OneToOne
    private CreditCard creditCard;
}
