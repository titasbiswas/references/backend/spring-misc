package pvt.titas.validation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pvt.titas.validation.domain.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
